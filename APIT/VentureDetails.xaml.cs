﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace APIT
{
    /// <summary>
    /// Interaction logic for VentureDetails.xaml
    /// </summary>
    public partial class VentureDetails : UserControl
    {

        public event EventHandler OpenSelectedVenture;
        public event EventHandler backToSelectedDist;
        public event EventHandler OpenContactUs;

        public VentureDetails()
        {
            InitializeComponent();

            double SWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double SHeight = System.Windows.SystemParameters.PrimaryScreenHeight - 67;
            brdVideoEnlarge.Height = SHeight;
            brdVideoEnlarge.Width = SWidth;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            VentureVideo.ScrubbingEnabled = true;
            //VentureVideo.Position = TimeSpan.FromMilliseconds(5);
            VentureVideo.Stop();
        }

        private void ScrollKeyPoints_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnPlay_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            btnPlay.Visibility = Visibility.Collapsed;
            btnPause.Visibility = Visibility.Visible;
            VentureVideo.Play();
        }

        private void btnPause_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideo.Pause();
            btnPause.Visibility = Visibility.Collapsed;
            btnPlay.Visibility = Visibility.Visible;
        }

        private void btnVolume_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideo.Volume = 0;
            volumeSlider.Value = 0;
            btnVolume.Visibility = Visibility.Collapsed;
            btnMute.Visibility = Visibility.Visible;
        }

        private void btnMute_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideo.Volume = 100;
            volumeSlider.Value = 100;
            btnMute.Visibility = Visibility.Collapsed;
            btnVolume.Visibility = Visibility.Visible;
        }

        private void VentureVideo_MediaOpened_1(object sender, RoutedEventArgs e)
        {
            timelineSlider.Maximum = VentureVideo.NaturalDuration.TimeSpan.TotalMilliseconds;
        }

        private void VentureVideo_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            VentureVideo.Stop();
        }

        // Change the volume of the media. 
        private void ChangeMediaVolume(object sender, RoutedPropertyChangedEventArgs<double> args)
        {
            VentureVideo.Volume = (double)volumeSlider.Value;
            if (VentureVideo.Volume == 0)
            {
                btnVolume.Visibility = Visibility.Collapsed;
                btnMute.Visibility = Visibility.Visible;
            }
            if (VentureVideo.Volume > 0)
            {
                btnMute.Visibility = Visibility.Collapsed;
                btnVolume.Visibility = Visibility.Visible;
            }
        }

        void InitializePropertyValues()
        {
            // Set the media's starting Volume and SpeedRatio to the current value of the 
            // their respective slider controls.
            VentureVideo.Volume = (double)volumeSlider.Value;
            VentureVideoEnlarge.Volume = (double)volumeSliderMax.Value;           
        }

        private void SeekToMediaPosition(object sender, RoutedPropertyChangedEventArgs<double> args)
        {
            int SliderValue = (int)timelineSlider.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, miniseconds. 
            // Create a TimeSpan with miliseconds equal to the slider value.
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            VentureVideo.Position = ts;
        }

        
        private void btnProjectMap_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            OpenSelectedVenture(this, null);
        }

        private void btnContactUs_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            OpenContactUs(this, null);
        }

        private void btnBacktoSelectedDist_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            backToSelectedDist(this, null);
        }

        private void btnVideoFullScreen_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideo.Stop();
            btnPlay.Visibility = Visibility.Visible;
            btnPause.Visibility = Visibility.Collapsed;
            brdVideoEnlarge.Visibility = Visibility.Visible;

            btnPlayMax.Visibility = Visibility.Collapsed;
            btnPauseMax.Visibility = Visibility.Visible;

            VentureVideoEnlarge.Play();
           
            Storyboard VideoSB = this.TryFindResource("VideoSB") as Storyboard;
            VideoSB.Begin();
        }

        private void btnPlayMax_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            btnPlayMax.Visibility = Visibility.Collapsed;
            btnPauseMax.Visibility = Visibility.Visible;
            VentureVideoEnlarge.Play();
        }

        private void btnPauseMax_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideoEnlarge.Pause();
            btnPauseMax.Visibility = Visibility.Collapsed;
            btnPlayMax.Visibility = Visibility.Visible;
        }

        private void btnVolumeMax_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideoEnlarge.Volume = 0;
            volumeSliderMax.Value = 0;
            btnVolumeMax.Visibility = Visibility.Collapsed;
            btnMuteMax.Visibility = Visibility.Visible;
        }

        private void btnMuteMax_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideoEnlarge.Volume = 100;
            volumeSliderMax.Value = 100;
            btnMuteMax.Visibility = Visibility.Collapsed;
            btnVolumeMax.Visibility = Visibility.Visible;
        }

        private void volumeSliderMax_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            VentureVideoEnlarge.Volume = (double)volumeSliderMax.Value;
            if (VentureVideoEnlarge.Volume == 0)
            {
                btnVolumeMax.Visibility = Visibility.Collapsed;
                btnMuteMax.Visibility = Visibility.Visible;
            }
            if (VentureVideoEnlarge.Volume > 0)
            {
                btnMuteMax.Visibility = Visibility.Collapsed;
                btnVolumeMax.Visibility = Visibility.Visible;
            }
        }
        
        //void InitializePropertyValuesMax()
        //{
        //    // Set the media's starting Volume and SpeedRatio to the current value of the 
        //    // their respective slider controls.
        //}

        private void timelineSliderMax_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int SliderValue = (int)timelineSliderMax.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, miniseconds. 
            // Create a TimeSpan with miliseconds equal to the slider value.
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            VentureVideoEnlarge.Position = ts;
        }

        private void btnVideoFullScreenMax_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            VentureVideoEnlarge.Stop();
            brdVideoEnlarge.Visibility = Visibility.Collapsed;            
        }

        private void VentureVideoEnlarge_MediaOpened_1(object sender, RoutedEventArgs e)
        {
            timelineSliderMax.Maximum = VentureVideoEnlarge.NaturalDuration.TimeSpan.TotalMilliseconds;
        }

        private void VentureVideoEnlarge_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            VentureVideoEnlarge.Stop();
            brdVideoEnlarge.Visibility = Visibility.Collapsed;            
        }
        

    }
}
