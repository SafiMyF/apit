﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace APIT
{
    /// <summary>
    /// Interaction logic for SelectedDist.xaml
    /// </summary>
    public partial class SelectedDist : UserControl
    {
        public SelectedDist()
        {
            InitializeComponent();
        }

        public event EventHandler backToHomeLayout;
        public event EventHandler OpenContactUs;
        public event EventHandler OpenVentureDetails;
        //public event EventHandler backToSelectedDist;

        private void btnBackToHomeLayout_PreviewTouchUp(object sender, TouchEventArgs e)
        {            
            backToHomeLayout(this, null);
        }

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            OpenContactUs(this, null);
        }

        private void btnMark1_TouchDown_1(object sender, TouchEventArgs e)
        {
            OpenVentureDetails(this, null);
            //OpenSelectedVenture(this, null);
        }
    }
}
