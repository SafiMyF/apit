﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace APIT
{
    /// <summary>
    /// Interaction logic for Policies.xaml
    /// </summary>
    public partial class Policies : UserControl
    {
        public Policies()
        {
            InitializeComponent();
            double width = System.Windows.SystemParameters.PrimaryScreenWidth;
            double height = System.Windows.SystemParameters.PrimaryScreenHeight;
            PolicieLayout.Width = width;
            PolicieLayout.Height = height;
        }

        //public event EventHandler myEvtClosePolicies;
        //private void btnClose_PreviewTouchUp(object sender, TouchEventArgs e)
        //{
        //    myEvtClosePolicies(this, null);
        //}
    }
}
