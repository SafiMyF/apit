﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace APIT
{
    /// <summary>
    /// Interaction logic for SelectedVenture.xaml
    /// </summary>
    public partial class SelectedVenture : UserControl
    {
        public SelectedVenture()
        {
            InitializeComponent();
        }

        public event EventHandler backToSelectedDist;
        public event EventHandler OpenContactUs;

        public delegate void mySelectedPlan(object sender, EventArgs e);
        public event mySelectedPlan myEventViewListItem;

        private void btnBackToHomeLayout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            backToSelectedDist(this, null);
        }

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            OpenContactUs(this, null);
        }

        private void btnViewListItem_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }

        private void Button_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }

        private void Button_TouchDown_2(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }

        private void Button_TouchDown_3(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }

        private void Button_TouchDown_4(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }
        
        private void SViewVentureList_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        bool isScrollMoved = false;

        private void SViewVentureList_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void SViewVentureList_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void SViewVentureList_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void SViewVentureList_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    myEventViewListItem(btnTouchedItem, null);
                }
            }
        }
        
    }
}
