﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace APIT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();            
        }
        int i = 20;

        private void btnHome_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            i = 100;
            HomeLayout.Visibility = Visibility.Visible;
            FooterPanel.Visibility = Visibility.Collapsed;
            parentPanel.Children.Clear();
        }

        private void btnSelectedArea_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LoadSelectedDist();            
        }


        void stdVentr_myEventViewListItem(object sender, EventArgs e)
        { 
            if (i == 100)
                i = 10;

            Button btnDets = sender as Button;
            if (btnDets != null)
            {
                PlanView PVw = new PlanView();
                PVw.PlanViewImg = btnDets;
                PVw.IsManipulationEnabled = true;
                PVw.RenderTransform = new MatrixTransform(1, 0, 0, 1, i + 20, i + 20);
                PVw.BringIntoView();
                Panel.SetZIndex(PVw, 9999);
                parentPanel.Children.Add(PVw);
                PVw.myEventClosePlanView += PVw_myEventClosePlanView;
                i = i + 10;
            }
        }

        void PVw_myEventClosePlanView(object sender, EventArgs e)
        {
            PlanView PVw = (PlanView)sender;
            parentPanel.Children.Remove(PVw);
        }

        void stdVentr_backToSelectedDist(object sender, EventArgs e)
        {
            loadVentureDetails();
        }

        void stdVentr_OpenContactUs(object sender, EventArgs e)
        {
            LoadContactUs();
        }

        private void LoadSelectedDist()
        {
            HomeLayout.Visibility = Visibility.Collapsed;
            FooterPanel.Visibility = Visibility.Visible;
            SelectedDist sDist = new SelectedDist();
            parentPanel.Children.Clear();

            sDist.grdMap.IsManipulationEnabled = true;
            sDist.grdMap.RenderTransform = new MatrixTransform(1, 0, 0, 1, 0, 0);
            //sDist.grdMap.BringIntoView();
            //Panel.SetZIndex(sDist.grdMap, 9999);

            parentPanel.Children.Add(sDist);
            sDist.backToHomeLayout += sDist_backToHomeLayout;
            sDist.OpenContactUs += sDist_OpenContactUs;
            sDist.OpenVentureDetails += sDist_OpenVentureDetails;
        }

        void sDist_OpenVentureDetails(object sender, EventArgs e)
        {
            loadVentureDetails();
        }

        private void loadVentureDetails()
        {
            FooterPanel.Visibility = Visibility.Collapsed;
            VentureDetails vDets = new VentureDetails();
            parentPanel.Children.Clear();
            parentPanel.Children.Add(vDets);

            //Raise Events from VentureDetails
            vDets.OpenSelectedVenture += vDets_OpenSelectedVenture;
            vDets.OpenContactUs += vDets_OpenContactUs;
            vDets.backToSelectedDist += vDets_backToSelectedDist;
        }

        void vDets_backToSelectedDist(object sender, EventArgs e)
        {
            LoadSelectedDist();
        }

        void vDets_OpenContactUs(object sender, EventArgs e)
        {
            LoadContactUs();
        }

        void vDets_OpenSelectedVenture(object sender, EventArgs e)
        {
            FooterPanel.Visibility = Visibility.Collapsed;
            SelectedVenture stdVentr = new SelectedVenture();
            parentPanel.Children.Clear();
            parentPanel.Children.Add(stdVentr);
            stdVentr.backToSelectedDist += stdVentr_backToSelectedDist;
            stdVentr.OpenContactUs += stdVentr_OpenContactUs;
            stdVentr.myEventViewListItem += stdVentr_myEventViewListItem;
        }

        void sDist_OpenContactUs(object sender, EventArgs e)
        {
            LoadContactUs();
        }

        private void LoadContactUs()
        {
            ContactUs Cu = new ContactUs();
            parentPanel.Children.Add(Cu);
            Cu.BringIntoView();
            Panel.SetZIndex(Cu, 9999);
            Cu.myEvtCloseContactUs += Cu_myEvtCloseContactUs;
        }

        void Cu_myEvtCloseContactUs(object sender, EventArgs e)
        {
            ContactUs Cu = (ContactUs)sender;
            parentPanel.Children.Remove(Cu);
        }

        void sDist_backToHomeLayout(object sender, EventArgs e)
        {
            i = 100;
            HomeLayout.Visibility = Visibility.Visible;
            FooterPanel.Visibility = Visibility.Visible;
            parentPanel.Children.Clear();
        }

        private void btnAwards_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            i = 100;
            Awards Awd = new Awards();
            parentPanel.Children.Clear();
            HomeLayout.Visibility = Visibility.Collapsed;
            parentPanel.Children.Add(Awd);
            FooterPanel.Visibility = Visibility.Visible;
        }

        private void btnPolicies_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            i = 100;
            Policies Plcy = new Policies();
            parentPanel.Children.Clear();
            HomeLayout.Visibility = Visibility.Collapsed;
            parentPanel.Children.Add(Plcy);
            FooterPanel.Visibility = Visibility.Visible;
        }

        private void btnPromotions_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            i = 100;
            Promotion Prmtn = new Promotion();
            parentPanel.Children.Clear();
            HomeLayout.Visibility = Visibility.Collapsed;
            parentPanel.Children.Add(Prmtn);
            FooterPanel.Visibility = Visibility.Visible;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.ManipulationStarting -= MainWindow_ManipulationStarting;
            this.ManipulationDelta -= MainWindow_ManipulationDelta;
            this.ManipulationStarting += MainWindow_ManipulationStarting;
            this.ManipulationDelta += MainWindow_ManipulationDelta;
        }

        ManipulationModes currentMode = ManipulationModes.All;
        void MainWindow_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            var element = args.OriginalSource as UIElement;

            var transformation = element.RenderTransform
                                                 as MatrixTransform;
            var matrix = transformation == null ? Matrix.Identity :
                                           transformation.Matrix;

            matrix.ScaleAt(args.DeltaManipulation.Scale.X,
                           args.DeltaManipulation.Scale.Y,
                           args.ManipulationOrigin.X,
                           args.ManipulationOrigin.Y);

            matrix.RotateAt(args.DeltaManipulation.Rotation,
                            args.ManipulationOrigin.X,
                            args.ManipulationOrigin.Y);

            matrix.Translate(args.DeltaManipulation.Translation.X,
                             args.DeltaManipulation.Translation.Y);

            element.RenderTransform = new MatrixTransform(matrix);
            args.Handled = true;      
        }

        void MainWindow_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);

            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }



        private void btnBackToHomeLayout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            i = 20;
            HomeLayout.Visibility = Visibility.Visible;
            FooterPanel.Visibility = Visibility.Collapsed;
            parentPanel.Children.Clear();
        }

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LoadContactUs();
        }
    }
}
